import axios from "axios";
import FullScreenWrapper from "./components/FullScreenWrapper";
import './main.scss';
import Product from "./components/Product";
import { useEffect, useState } from "react";
import Header from "./components/Header";


const style = {
  display: 'flex',
  flexWrap: 'wrap',
  justifyContent: 'center'
}

const handleClick = (event) => {
  console.log(event.target)
}



function App() {
  const [favoriteCount, setFavoriteCount] = useState(0);
  const [cartCount, setCartCount] = useState(0);
  const [products, setProducts] = useState([]);

  const [selectedCardId, setSelectedCardId] = useState(null);


  useEffect(() => {

    axios.get('/Products.json')
      .then((res) => setProducts(res.data))
      .catch((err) => console.log(err))
      
      // localStorage.setItem('favoriteCount', JSON.stringify(isFavorite));
  }, [])

  const updateFavoriteCount = (countChange) => {
    setFavoriteCount(favoriteCount + countChange);
  };

  const updateCartCount = () => {
    setCartCount(cartCount + 1);
  };

  const removeProduct = (id) => {
    const deleteCard = window.confirm('Delete this card?');
    if(deleteCard === true){
      setProducts(products.filter(p => p.id !== id))
    }
    else{
      setProducts(products)
    }
    

  }

  useEffect(() => {
    // Зберегти обраний ID у localStorage, коли він змінюється
    if (selectedCardId !== null) {
      localStorage.setItem('selectedCardId', selectedCardId);
    }
  }, [selectedCardId]);

  const checkProductId = (id) => {
    setSelectedCardId(id);
    console.log(id)
  }


  const productsElements = products.map(p => <Product key={p.id} info={p} updateFavoriteCount={updateFavoriteCount} updateCartCount={updateCartCount} handleClick={handleClick} deleteHandler={removeProduct} checkProductId={checkProductId} />)

  return (
    <FullScreenWrapper>
      <Header favoriteCount={favoriteCount} cartCount={cartCount} />
      <div style={style}>
        {productsElements}
      </div>
    </FullScreenWrapper>
  );
}

export default App;
