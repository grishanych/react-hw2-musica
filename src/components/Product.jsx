import {  useEffect, useState } from "react";
import Button from './Button';
import styles from './styles/Products.module.scss'
import style from './styles/Modal.module.scss'
import Modal from "./Modal";
import PropTypes from 'prop-types';



function Product(props){

  const [showModal, setShowModal] = useState(false);
  const [isFavorite, setIsFavorite] = useState(false);
  const [isInCart, setisInCart] = useState(false);


  useEffect(() => {
    localStorage.setItem('favoriteCount', JSON.stringify(isFavorite));
    localStorage.setItem('isInCart', JSON.stringify(isInCart));
    }, [isFavorite, isInCart]);

    
  const handleFavoriteClick = () => {
    setIsFavorite(!isFavorite);
    props.updateFavoriteCount(isFavorite ? -1 : 1); // Додати або зменшити кількість обраних товарів
    // localStorage.setItem('isFav', !isFavorite)
  };

  // const handleFavoriteClick = () => {
  //   const newFavoriteStatus = !isFavorite;
  //   setIsFavorite(newFavoriteStatus);
  //   updateFavoriteCount(newFavoriteStatus ? 1 : -1);
  //   localStorage.setItem(`favoriteStatus_${id}`, newFavoriteStatus.toString());
  // };

  const handleAddToCartClick = () => {
    setisInCart(!isInCart);
    props.updateCartCount(isInCart + 1 ); // Додати або зменшити кількість обраних товарів
  };

  // const handleAddToCartClick = () => {
  //   const newCartStatus = !isInCart;
  //   setisInCart(newCartStatus);
  //   updateCartCount(newCartStatus ? 1 : -1);
  //   localStorage.setItem(`cartStatus_${id}`, newCartStatus.toString());
  // };


  const openModal = () => {
    setShowModal(true)
  }

  const cancelButton = (
    <button className={style.modalButton} onClick={() => {
      
      setShowModal(false)
    }}>Cancel</button>
  );

  const saveButton = (
    <button className={style.modalButton} 
    onClick= {() => {handleAddToCartClick(); setShowModal(false)}}
      >Ok</button>
  );

  const actionButtons = [saveButton, cancelButton];


    return(
      <>
        <div className={styles.card}> 
        
        <button className={`${styles.favoriteButton} ${isFavorite ? styles.favorite : styles.star}`} onClick={()=> {handleFavoriteClick(); props.checkProductId(props.info.id)}}>
        <span>&#9733;</span>
      </button>
                <h2>{props.info.brandName}</h2>
                <p>{props.info.price} $</p>
                <img className={styles.imgCar} src={props.info.image} alt="" />
                <Button backgroundColor='lightgreen' text='Add to cart' heandler={openModal} />    
                <button className={styles.deleteBtn} onClick={() => props.deleteHandler(props.info.id)}>Delete Card</button>           
        </div>
        
        <Modal 
      header='Add to cart this product?'
      text='Are you sure you want to  cart this product?' 
      actions={actionButtons}
      showModal={showModal}
      setShowModal={setShowModal}
      closeButton={() => setShowModal(false)}
    />
        </>
        
    )
}

Product.propTypes= {
  info: PropTypes.shape({
    id: PropTypes.number.isRequired,
    brandName: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired
  }).isRequired,
  updateFavoriteCount: PropTypes.func.isRequired,
  updateCartCount: PropTypes.func.isRequired,
  handleClick: PropTypes.func.isRequired,
  deleteHandler: PropTypes.func.isRequired,
  checkProductId: PropTypes.func.isRequired
}

Product.defaultProps = {
  updateFavoriteCount: () => {},
  updateCartCount: () => {},
  handleClick: () => {},
  deleteHandler: () => {},
  checkProductId: () => {},
};

export default Product