import PropTypes from 'prop-types';

import React from 'react';
import Cart from './images/cart.png';
import Favorite from './images/favorite.png';
import styles from './styles/Header.module.scss';

function Header({ favoriteCount, cartCount }) {
  return (
    <header >
      <nav >
        <ul className={styles.nav}>
          <li>
            <img src={Favorite} alt="Favorite" /> ({favoriteCount})
          </li>
          <li>
          <img src={Cart} alt="Cart" /> ({cartCount})
          </li>
        </ul>
      </nav>
    </header>
  );
}

Header.propTypes= {
  favoriteCount: PropTypes.number.isRequired,
  cartCount: PropTypes.number.isRequired
}

export default Header;
