import styles from './styles/Buttons.module.scss';
import PropTypes from 'prop-types';

function Button(props) {


    return (
        <button onClick={props.heandler}
            style={{ backgroundColor: props.backgroundColor }}
            className={styles.button}>{props.text}</button>

    )
}

Button.propTypes= {
    heandler: PropTypes.func.isRequired,
    backgroundColor: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired
  }

export default Button